var express = require('express');
var bodyParser = require('body-parser');
const request = require('request');

var app = express();

app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));

// ROUTES
app.get('/', function (req, res) {
    res.render('home', {
        
    });
});

app.post('/', function (req, res) {
    var user = {
        "name" : req.body.name,
        "job" : req.body.job
    };

    request.post({
        url: 'https://reqres.in/api/users', 
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        body: require('querystring').stringify(user)
    },

    function (error, response, body) {
        console.log(body);
        //console.log(error);
    });
});

app.listen(3051, function () {
    console.log('Exercise running on the port 3050');
});