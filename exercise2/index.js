var express = require('express');
var bodyParser = require('body-parser');
var myFunction = require('./functions');

var app = express();

app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));

/*app.get('/', function (req, res) {
    res.render('home',{
        item: "Lorem Ipsum"
    });
  }); */

app.get('/', function (req, res) {
  res.send("Hello, you're welcome!");
}); 

app.get('/user/:name', function (req, res) {
    console.log("Hello, " + myFunction.printName(req.params.name));
  });

app.get('/sum/:number', function (req, res) {
    square = myFunction.calculateSquare(req.params.number);
    res.write(square.toString());
});

app.listen(8081, function () {
  console.log('Exercise running on the port 8081');
});