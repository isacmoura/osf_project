var express = require('express');
var bodyParser = require('body-parser');
var functions = require('./functions');


var app = express();

app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));

// ROUTES
app.get('/number/:num', function (req, res) {
    res.write(functions.oddOrEven(req.params.num));
});

app.get('/random/:number', function (req, res) {
    res.render('home', {
        item: functions.generateRandom(req.params.number)
    });
});

app.get('/print/:number', function (req, res) {
    if(req.params.number < 5) {
        res.render('home', {
            num: req.params.number
        });
    }else{
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end(req.params.number);
    }
});

app.get('/home', function(req, res) {
    var array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    res.render('home', {
        array: array
    });
});

app.listen(3050, function () {
    console.log('Exercise running on the port 3050');
});