function oddOrEven(num) {
    if (num % 2 == 0) {
        return 'Even';
    }else if(num % 2 == 1) {
        return 'Odd';
    }else{
        return console.error();
    }
}

/*Here we receive a number to generate our random. 
I'm using Math.floor() because the Math.random only returns a random
from 0 and 1 range */
function generateRandom(number) {
    return Math.floor(Math.random() * number);
}



module.exports = {
    oddOrEven,
    generateRandom
}