function printName(name) {
    return name;
}

function calculateSquare(number) {
    return number * number;
}

module.exports = {
    printName,
    calculateSquare
}