var express = require('express');
var bodyParser = require('body-parser');
const request = require('request');

var app = express();

app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));

// ROUTES

app.get('/api/users/:id', function (req, res) {
    request(`https://reqres.in/api/users/${req.params.id}`, function (error, response, body) {
        if (response.statusCode == 200){
            response.send(body);
        }else{
            console.log(error);
        }
        
    });
});

app.get('/api/unknown/', function (req, res) {
    request(`https://reqres.in/api/unknown/`, function (error, response, body) {
        if (response.statusCode == 200){
            response.send(body);
        }else{
            console.log(error);
        }
    });
});

app.get('/api/unknown/:page', function (req, res) {
    request.get(`https://reqres.in/api/unknown/${req.params.page}`, function (error, response, body) {
        if (response.statusCode == 200){
            response.send(body);
        }else{
            response.send(error);
        }
        
    });
});

app.get('/', function (req, res) {
    res.render('home', {
        url: 'https://reqres.in/api/users'
    });
    
    var user = {
        "name" : "Isac",
        "job" : "Student"
    };

    request.post({
        url: 'https://reqres.in/api/users', 
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        body: require('querystring').stringify(user)
    },

    function (error, response, body) {
        if ((response.statusCode >= 200) && (response.statusCode <= 300)){
            console.log(body);
        }else{
            console.log(error);
        }
    });
});

app.put('/api/user/:id', function (req, res) {

    var user = {
        "name" : "Isac",
        "job" : "Student"
    };

    request.put({
        url: `https://reqres.in/api/users/${req.params.id}`,
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        body: require('querystring').stringify(user)
    },

    function (error, response, body){
        if ((response.statusCode >= 200) && (response.statusCode <= 300)){
            console.log(body);
        }else{
            console.log(error);
        }
    });
});

app.delete('/api/delete/:id',  function (req, res) {
    var id = req.params.id;

    request.delete({
        url: `https://reqres.in/api/users/${id}`,
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        body: require('querystring').stringify(id)
    },
    
    function (error, response, body) {
            console.log(id);
    });
});

app.get('/api/login', function (req, res) {
    var user = {
        "email" : "isac@isac.com",
        "password" : "cityslicka"
    };

    request.post({
        url: 'https://reqres.in/api/login', 
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        body: require('querystring').stringify(user)
    },

    function (error, response, body) {
        if ((response.statusCode >= 200) && (response.statusCode <= 300)){
            console.log(body);
        }else{
            console.log(error);
        }
    });
});

app.get('/api/loginunsucess', function (req, res) {
    var user = {
        "email" : "isac@isac.com",
    };

    request.post({
        url: 'https://reqres.in/api/login', 
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        body: require('querystring').stringify(user)
    },

    function (error, response, body) {
        console.log(body);
        console.log(error);
        
    });
});

app.listen(4000, function () {
    console.log('Exercise running on the port 4000');
});